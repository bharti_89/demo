package com.example.bharti.demo;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Signup extends Activity implements View.OnClickListener{

    ImageView ivBackSignUp;
    EditText etFullName, etEmail, etPassword;
    Button bCreateUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signup);

        etFullName = (EditText) findViewById(R.id.etFullName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bCreateUser = (Button) findViewById(R.id.bCreateUser);
        ivBackSignUp = (ImageView) findViewById(R.id.ivBackSignUp);

        bCreateUser.setOnClickListener(this);
        ivBackSignUp.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bCreateUser:
                break;
            case R.id.ivBackSignUp:
                startActivity(new Intent(this, Home.class));
                break;
        }
    }

    public void onButtonTap(View v){
        Toast t = Toast.makeText(getApplicationContext(), "Smart City Governance", Toast.LENGTH_LONG);
        t.show();
    }
}
