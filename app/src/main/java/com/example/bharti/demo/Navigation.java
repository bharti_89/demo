package com.example.bharti.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Navigation extends Activity implements View.OnClickListener{

    ImageView ivDashboard, ivNewsFeed, ivIssue, ivMessenger;
    TextView tvDashboard, tvNewsFeed, tvIssueIcon, tvMessenger;
    Button bLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_navigation);

        tvDashboard = (TextView) findViewById(R.id.tvDashboard);
        tvNewsFeed = (TextView) findViewById(R.id.tvNewsFeed);
        tvIssueIcon = (TextView) findViewById(R.id.tvIssueIcon);
        tvMessenger = (TextView) findViewById(R.id.tvMessenger);
        bLogout = (Button) findViewById(R.id.bLogOut);
        ivDashboard = (ImageView) findViewById(R.id.ivDashboard);
        ivNewsFeed = (ImageView) findViewById(R.id.ivNewsFeed);
        ivIssue = (ImageView) findViewById(R.id.ivIssue);
        ivMessenger = (ImageView) findViewById(R.id.ivMessenger);

        tvDashboard.setOnClickListener(this);
        tvNewsFeed.setOnClickListener(this);
        tvIssueIcon.setOnClickListener(this);
        tvMessenger.setOnClickListener(this);
        bLogout.setOnClickListener(this);
        ivDashboard.setOnClickListener(this);
        ivNewsFeed.setOnClickListener(this);
        ivIssue.setOnClickListener(this);
        ivMessenger.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.tvDashboard:
                break;
            case R.id.tvNewsFeed:
                break;
            case R.id.tvIssueIcon:
                startActivity(new Intent(this, Issue.class));
                break;
            case R.id.tvMessenger:
                break;
            case R.id.bLogOut:
                startActivity(new Intent(this, Login.class));
                break;
            case R.id.ivDashboard:
                break;
            case R.id.ivNewsFeed:
                break;
            case R.id.ivIssue:
                startActivity(new Intent(this, Issue.class));
                break;
            case R.id.ivMessenger:
                break;
        }
    }
}
