package com.example.bharti.demo;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Login extends Activity implements View.OnClickListener{

    ImageView ivBackLogin;
    EditText etUsername, etLoginPassword;
    Button bLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        etUsername = (EditText) findViewById(R.id.etEmailId);
        etLoginPassword = (EditText) findViewById(R.id.etLoginPassword);
        bLogin = (Button) findViewById(R.id.bLogin);
        ivBackLogin = (ImageView) findViewById(R.id.ivBackLogin) ;

        bLogin.setOnClickListener(this);
        ivBackLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.ivBackLogin:
                startActivity(new Intent(this, Home.class));
                break;
            case R.id.bLogin:
                startActivity(new Intent(this, Navigation.class));
                break;
        }
    }
}
