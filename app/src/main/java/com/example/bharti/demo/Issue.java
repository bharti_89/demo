package com.example.bharti.demo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Issue extends Activity implements View.OnClickListener {

    ImageView ivBackIssue;
    EditText etDate, etDescription, etLocation;
    Button bCreateIssue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_issue);

        etDate = (EditText) findViewById(R.id.etDate);
        etDescription = (EditText) findViewById(R.id.etDescription);
        etLocation = (EditText) findViewById(R.id.etLocation);
        bCreateIssue = (Button) findViewById(R.id.bCreateIssue);
        ivBackIssue = (ImageView) findViewById(R.id.ivBackIssue);

        bCreateIssue.setOnClickListener(this);
        ivBackIssue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bCreateIssue:
                break;
            case R.id.ivBackIssue:
                startActivity(new Intent(this, Navigation.class));
                break;
        }
    }
}
